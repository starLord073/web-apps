function validateJSON(data=null){
    console.log("clicked");
    const textArea = document.getElementById("inputText");
    try {
        var toValidate;
        if(data){
            toValidate = JSON.parse(textArea.value);
        }else{
            toValidate = JSON.stringify(textArea.value);
        }
        writeToOputput(toValidate , true)
        textArea.value="";

    } catch (error) {
        writeToOputput(error);
    }

}

function writeToOputput(text, copy=false) {
    const outputText = document.getElementById("outputText");
    outputText.value = text;
    outputText.style.visibility="visible";
    if(copy){
        outputText.select();
        document.execCommand("copy");
    }
}